typeset -Ag err_glyph
if [[ $ZSH_PROMPT_GLYPHS = 'emoji' ]]
then
    err_glyph=(
	0 '%B➤%b '
	1 '%{💥%2G%}'
	2 '%{📝%2G%}'
	130 '%{🛑%2G%}'
	146 '%{🛏%G%} '
    )
    err_glyph_default='⚠️ '
elif [[ $ZSH_PROMPT_GLYPHS = 'symbols' ]]
then
    err_glyph=(
	0 '%B  %b➤ '
	1 '%B☒ %b➤ '
	2 '%B⚠︎ %b➤ '
	130 '%B⎔ %b➤ '
	146 '%B☽ %b➤ '
    )
    err_glyph_default='%B☹︎ ➤%b '
else
    err_glyph=(
	0 '%B  %b%% '
	1 '%BX %b%% '
	2 '%Bx %b%% '
	130 '%B! %b%% '
	146 '%B& %b%% '
    )
    err_glyph_default='%B? %b%% '
fi

function git_status_and_branch() {
  branch=$(git branch --show-current 2>/dev/null) || return
  echo -n "$(git_prompt_status) %K{#444444} $branch %k%{$reset_color%}"
}

if (( $UID == 0 || $EUID == 0 ))
then
    PROMPT='%F{red}%B%n%b%f'
else
    PROMPT='%n'
fi
PROMPT+='%F{black}%B@%b%f%m'
PROMPT+='%F{black}%B:%b%f'
PROMPT+='%F{blue}%B${${${PWD#$HOME/}#$HOME}:-~}%b%f '
PROMPT+='${err_glyph[$?]:-$err_glyph_default} '
RPS1='$(git_status_and_branch)'

ZSH_THEME_GIT_PROMPT_PREFIX="%K{blue}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%k"

if [[ $ZSH_PROMPT_GLYPHS = 'emoji' ]]
then
    ZSH_THEME_GIT_PROMPT_ADDED='%{🧮%2G%}'
    ZSH_THEME_GIT_PROMPT_DELETED='%{🪓%2G%}'
    ZSH_THEME_GIT_PROMPT_MODIFIED='%{🛠%G%} '
    ZSH_THEME_GIT_PROMPT_RENAMED='%{✏️%G%} '
    ZSH_THEME_GIT_PROMPT_UNMERGED='%{⚔️%G%} '
    ZSH_THEME_GIT_PROMPT_UNTRACKED='%{🚧%2G%}'
elif [[ $ZSH_PROMPT_GLYPHS = 'symbols' ]]
then
    ZSH_THEME_GIT_PROMPT_ADDED='✚'
    ZSH_THEME_GIT_PROMPT_DELETED='✂︎'
    ZSH_THEME_GIT_PROMPT_MODIFIED='✑'
    ZSH_THEME_GIT_PROMPT_RENAMED='⎌'
    ZSH_THEME_GIT_PROMPT_UNMERGED='⦿'
    ZSH_THEME_GIT_PROMPT_UNTRACKED='☐'
else
    ZSH_THEME_GIT_PROMPT_ADDED='+'
    ZSH_THEME_GIT_PROMPT_DELETED='-'
    ZSH_THEME_GIT_PROMPT_MODIFIED='~'
    ZSH_THEME_GIT_PROMPT_RENAMED='@'
    ZSH_THEME_GIT_PROMPT_UNMERGED='*'
    ZSH_THEME_GIT_PROMPT_UNTRACKED='?'
fi

# vim:ft=zsh
