# Source all the start-up files.
function {
    local _init_dir
    local _file
    for _init_dir in $HOME/.zsh/init.d $HOME/.zsh-local/init.d
    do
	[[ -d $_init_dir ]] || continue
	for _file in $_init_dir/*.zsh(N)
	do
	    source $_file
	done
    done
}
