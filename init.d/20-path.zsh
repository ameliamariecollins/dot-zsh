
# NOTE: desired paths are PREPENDED to $path

function {
    local desired_paths=()

    #-- PDP-11 Cross Compiler
    desired_paths+=/usr/local/cross-pdp-11/bin

    #-- MacTeX
    desired_paths+=/Library/TeX/texbin

    #-- PlatformIO
    #desired_paths+=$HOME/.platformio/penv/bin
    #desired_paths+=$HOME/.platformio/packages/tool-avrdude/bin
    #desired_paths+=$HOME/.platformio/packages/toolchain-atmelavr/bin
    #desired_paths+=$HOME/.platformio/packages/toolchain-gccarmnoneeabi/bin

    # Prefer GNU userspace
    if [[ -d /opt/homebrew/opt ]]
    then
      for GNUBIN_DIR in /opt/homebrew/opt/*/libexec/gnubin
      do
          # [[ $GNUBIN_DIR = *coreutils* ]] && continue
          desired_paths+=$GNUBIN_DIR
      done
    fi

    # Prefer Arm Toolchain
    desired_paths+=/Applications/ArmGNUToolchain/14.2.rel1/arm-none-eabi/bin

    # Projects
    desired_paths+=$HOME/project/sysadmin-tools/bin
    desired_paths+=/srv/project/sysadmin-tools/bin

    # Prefer GHCup Haskell bins to homebrew ones
    desired_paths+=$HOME/.ghcup/bin

    # Homedir bin directory in front
    desired_paths+=$HOME/bin

    # Keep only first unique occurence of value. Make path a global variable.
    typeset -Ug path

    # Prepend desired paths
    local desired_path
    for desired_path in $desired_paths
    do
	[[ -d $desired_path ]] && path[1,0]=$desired_path
    done
}

# vim:ft=zsh
