autoload -Uz compinit
compinit

#compdef cargo
# if command -v rustc >/dev/null 2>&1; then
# 	source "$(rustc --print sysroot)"/share/zsh/site-functions/_cargo
# fi

# PlatformIO Core completion support
which pio >/dev/null 2>&1 && eval "$(_PIO_COMPLETE=zsh_source pio)"

# Load Angular CLI autocompletion.
which ng >/dev/null 2>&1 && source <(ng completion script)
