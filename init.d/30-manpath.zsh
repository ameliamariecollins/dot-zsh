
# NOTE: desired manpaths are PREPENDED to $manpath

function {
    local desired_manpaths=()

    # Man pages from XCode command line tools and latest SDK
    local xcode_command_line_tools="/Library/Developer/CommandLineTools"
    if [[ -d "$xcode_command_line_tools/usr/share/man" ]]
    then
	desired_manpaths+="$xcode_command_line_tools/usr/share/man"
    fi
    local xcode_command_line_tools_sdks="$xcode_command_line_tools/SDKs"
    if [[ -d $xcode_command_line_tools_sdks ]]
    then
	local xcode_man="$(find $xcode_command_line_tools_sdks -name man | sort -n | tail -1)"
	if [[ $xcode_man ]]
	then
	    desired_manpaths+=$xcode_man
	fi
    fi

    #-- Homebrew on Apple Silicon
    # Without "g" prefix
    local man_dir
    if [[ -d /opt/homebrew/opt ]]
    then
	for man_dir in /opt/homebrew/Cellar/*/*/share/man
	do
	    desired_manpaths+=$man_dir
	done
    fi

    # With "g" prefix
    local gnubin_man_dir
    if [[ -d /opt/homebrew/opt ]]
    then
	for gnubin_man_dir in /opt/homebrew/cellar/*/*/libexec/gnuman
	do
	    desired_manpaths+=$gnubin_man_dir
	done
    fi

    # Keep only first unique occurence of value. Make path a global variable.
    typeset -Ug manpath

    # Prepend desired paths
    local desired_manpath
    for desired_manpath in $desired_manpaths
    do
	[[ -d $desired_manpath ]] && manpath[1,0]=$desired_manpath
    done
}
