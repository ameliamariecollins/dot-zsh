# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
if [[ $TERM = linux ]]
then
    [[ -f ~/.p10k-console.zsh ]] && source ~/.p10k-console.zsh
else
    [[ -f ~/.p10k.zsh ]] && source ~/.p10k.zsh
fi

case $TERM in
    linux) P10K_ZSH=~/.p10k-console.zsh ;;
    *) P10K_ZSH=~/.p10k.zsh ;;
esac
[[ -f $P10K_ZSH ]] && source $P10K_ZSH
