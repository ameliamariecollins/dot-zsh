function {
    local LS_COLORS_FILE="$HOME/.zsh/LS_COLORS"
    test -e $LS_COLOR_FILE || return

    if type gdircolors >/dev/null
    then
	local DIRCOLORS='gdircolors'
    elif type dircolors >/dev/null
    then
	local DIRCOLORS='dircolors'
    else
    	return
    fi

    eval $($DIRCOLORS $LS_COLORS_FILE)
    alias vilscolors="vim $LS_COLORS_FILE; eval \$($DIRCOLORS $LS_COLORS_FILE)"

    if [[ -e /opt/homebrew/bin/gls ]]
    then
      alias ls="/opt/homebrew/bin/gls --classify=auto --color=auto"
      alias gls="/opt/homebrew/bin/gls --classify=auto --color=auto"
    else
      if uname | grep -q Darwin
      then
          alias ls="/bin/ls --color=auto -F"
      elif uname | grep -q Linux
      then
          alias ls="/bin/ls -F --color=auto"
      fi
    fi
}
