# Enable Powerlevel10k instant prompt. Should be executed as early as possible.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must be executed first.
typeset -g POWERLEVEL9K_INSTANT_PROMPT=on
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
