
#-- For ZSH
setopt extendedglob
setopt histfindnodups
setopt histignorealldups
setopt histreduceblanks
setopt kshglob
setopt zle
export ZSH_PROMPT_GLYPHS="unicode" # Choices: emoji, symbols (unicode), undefined (ASCII only)
export ZSH_DISABLE_COMPFIX="true" # Disable warnings about user-owned Homebrew ZSH extensions
export HISTFILE=$HOME/.zsh_history
export HISTSIZE='10000'
export SAVEHIST=$HISTSIZE

fpath=(
    $HOME/.zsh/completions
    $HOME/.zsh/repo/zsh-completions/src
    /opt/homebrew/share/zsh/functions
    /opt/homebrew/share/zsh/site-functions
    mac-zsh-completions
    $fpath
)


# Cargo
[[ -e "$HOME/.cargo/env" ]] && source "$HOME/.cargo/env"

# Homebrew
function {
    local _brew_bin=/opt/homebrew/bin/brew
    [[ -e $_brew_bin ]] && eval $($_brew_bin shellenv)
}

# Less
which lesspipe.sh >/dev/null 2>&1 && lesspipe.sh|source /dev/stdin
which bat >/dev/null 2>&1 && export LESSCOLORIZER=bat

# iTerm
test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

# Fallback prompt in case P10K deosn't load
export PS2="... "
if [ "$UID" = 0 ]; then
    export PS1='%m:%~%# '
else
    export PS1="%n@%m:%~%# "
fi

# Miscellaneous
export EDITOR="vim"
export F18_FC=gfortran-mp-devel
export GPG_TTY=$(tty)
export XAUTHORITY=$HOME/.Xauthority
mailpath=/var/mail/$USERNAME

