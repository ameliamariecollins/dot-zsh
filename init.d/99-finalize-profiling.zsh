# Finalize profiling if active.
if [[ $PROFILE_ZSH ]]
then
    unsetopt XTRACE
    exec 2>&3 3>&-
fi
