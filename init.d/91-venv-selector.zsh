# Python virtual environment selector.
# usage:  venv project_name
# map format: Per line, "project_name venv_dir" with arbitrary whitespace.

: "${VENV_MAP:=$HOME/.venv.map}"

venv() {
    # Does a virtual environment map exist?
    if ! [[ -e $VENV_MAP ]]; then
        echo "Virtual environment map not found: $VENV_MAP"
        return 1
    fi

    # Was a project name supplied?
    if ! [[ $1 ]]; then
        echo "Usage: venv project_name"
        return 2
    fi

    # Can the project name be found in the map?
    local _project_dir _venv_dir _old_venv
    while read -r _project_name _venv_dir; do
        [[ $_project_name ]] || continue # Skip blank lines
        [[ $_project_name == \#* ]] && continue # Skip comments
        if [[ $1 == "$_project_name" ]]; then
            # Has some other former virtual environment been activated?
            if [[ $VIRTUAL_ENV ]]; then
                _old_venv="$VIRTUAL_ENV"
                deactivate
                if [[ $VIRTUAL_ENV ]]; then
                    echo -n "Could not deactivate active virtual environment "
                    echo "at $VIRTUAL_ENV"
                    return 1
                else
                    echo "Deactivated former virtual environment at $_old_venv"
                fi
            fi
            eval "source $_venv_dir/bin/activate" # Interpolate shell vars, &c.
            return 0
        fi
    done < "$VENV_MAP"
    echo "Could not find a project named $_project_name in $VENV_MAP"
    return 1
}
